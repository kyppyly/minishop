<?php
Route::get ( '/', array (
'as' => 'home',
'uses' => 'HomeController@home'
		) );
/*
 * Authentication group
 */
Route::group(array('before'=>'auth', 'namespace' => 'Admin'), function (){
	Route::group ( array ('before' => 'csrf'), function () {	
		
		
	/*
	 * Use for product action
	 */
	Route::post('admin/product-update/{id}', array(
		'as'=>'product-update',
		'uses'=>'ProductController@updateProduct'
	));
	
	Route::post('admin/prduct-manager/product-addnew', array(
				'as'=>'addnew-product',
				'uses'=>'ProductController@addnewProduct'
	));
	
	Route::post('admin/product-manager/addnew', array(
			'as'=>'post-product-addnew',
			'uses'=>'ProductController@insertProduct'
	));
	
	/////////////////////////////////////////////////////////////////////
});
	
	
/////////    Get all method for product action /////////////////////////
Route::get('admin/product-manager', array(
		'as'=>'product-manager',
		'uses'=>'ProductController@listProduct'
));

Route::get('admin/product-manager/addnew', array(
			'as'=>'get-addnew-product',
			'uses'=>'ProductController@addnewProduct'
));
Route::get('admin/product-manager/delete/{id}', array(
			'as'=>'get-delete-product',
			'uses'=>'ProductController@removeProduct'
));
Route::get('admin/product-manager/edit/{id}',array(
			'as'=>'get-edit-product',
			'uses'=>'ProductController@editProduct'
));

///////  End for product  ////////////////////////////////////

/// Admin logout /////////////////
Route::get('/admin/admin-logout', array(
		'as'=>'admin-logout',
		'uses'=>'AccountController@getLogOut'
	));
});



/*
 * Unauthenticated group
 */
Route::group ( array ('before' => 'guest' ,'namespace' => 'Admin'), function () {
	/*
	 * CSRF protection
	 */
	Route::group ( array ('before' => 'csrf' ), function () {
		Route::post('admin/admin-login', array(
			'as'=>'post-admin-login',
			'uses'=>'AccountController@postLogin'
		));
	} );
	
	/*
	 * Signin (GET)
	 */
		Route::get('admin/admin-login', array(
		'as'=>'get-admin-login',
		'uses'=>'AccountController@getLogin'
		));


} );

Route::get('/add-item-tocart/{id}', array(
		'as'=> 'addToCart',
		'uses'=>'HomeController@addCart'
	));


Route::resource('Test', 'TestController');


Route::resource('orders', 'OrdersController');

Route::resource('students', 'StudentsController');

Route::resource('lops', 'LopsController');

Route::resource('admin/computers', 'ComputerController'); // admin