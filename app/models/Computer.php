<?php

class Computer extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'computers';
	protected $guarded = ['id', 'created_at', 'update_at'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');Computer::with('pcmaker')->get()
	public function pcmaker(){
		return $this->belongsTo('PCMaker', 'conputer_maker_id', 'id');
	}
}
