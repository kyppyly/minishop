<?php

class Bill extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'bills';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');

	public function computer(){
		return $this->belongToMany('Computer', 'com_id', 'id');
	}


	public function customer(){
		return $this->belongTo('Customer');
	}

	public function customer(){
		return $this->belongTo('User');
	}
}
