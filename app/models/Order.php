<?php

class Order extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'code' => 'required'
	);
}
