<?php
class Customer extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customers';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');

	public function bill(){
		return $this->hasMany('Bill', 'cus_id', 'id');
	}
}
