<?php

class PCMaker extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'computer_makers';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');
	public function computer(){
		return $this->hasMany('Computer', 'computer_makers', 'id');
	}
}
