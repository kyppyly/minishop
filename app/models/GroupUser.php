c<?php
class GroupUser extends Eloquent{


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'group_users';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');

	public function user(){
		return $this->hasMany('User', 'group_user_id', 'id');
	}
}
