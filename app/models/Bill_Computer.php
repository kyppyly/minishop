<?php
class Bill_Computer extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'bill_computers';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');
	public function computer(){
		return $this->hasMany('Computer', 'computer_makers', 'id');
	}
}