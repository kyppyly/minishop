<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;
use Auth;
class AccountController extends \BaseController{

	public function getLogin(){
		return View::make('admin.login');

	}

	public function getLogOut(){
		\Auth::logout();
		return Redirect::route('get-admin-login');
	}
	public function postLogin(){
		$input = Input::all();
		$input['username'] = trim($input['username']);
		$validator = Validator::make($input, array(
				'username'=>'required',
				'password'=>'required'
			));

/*
	if input is fail then redirect to view login
*/
		if($validator->fails()) {
			// Redirect to the sign in page
			return Redirect::route('get-admin-login')
				->withErrors($validator)
				->withInput();   // redirect the input
		}else{
			$remember = Input::has('remember')? true: false;
			$account = Input::get('username');
			$password = Input::get('password');
			$auth = \Auth::attempt(array(
					'account'=>$account,
					'password'=>$password,
					'actived'=>1
				), $remember);

		$remember = Input::get('remember', false);
		//if remember checkbox is checked then we set cookies to remember this account
		if($remember){
			$remember = \Auth::getRecallerName();
			\Cookie::queue($remember, \cookie::get($remember), 86400); //set for cookies availble to 2 days

		}
		//then authentication redirect to route
		if($auth){
			return \View::make("admin.main");
		}else{ //authentication is fail
			return Redirect::route('get-admin-login')
					->with('global', 'Email or Password is incorrect!');
		}
		
	}
	return Redirect::route('get-admin-login')->with('global', 'has a error, try again please!');
}
}