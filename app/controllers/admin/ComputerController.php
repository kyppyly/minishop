<?php

define ( "DESTINATION_PATH", public_path () . '/image_sp/' );
class ComputerController extends \BaseController {

	protected $computer;
	public function __construct(Computer $computer){
		$this->computer = $computer;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
			$mComputer = $this->computer->	with ( 'pcmaker' )->get ();
		if ($mComputer !== null) {
			return View::make ( "admin.product" )->with ( 'mComputer', $mComputer );
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$maker = PCMaker::all ()->last ();
		$maker = $maker->lists ( 'name', 'id' );
		return View::make('admin.addnew', compact('maker'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
					if (Input::hasFile ( 'image' )) {
				$file = Input::file ( 'image' );
				if ($file->isValid ()) {
					$rules = array (
							'file' => 'image|mime:jpg,gif,png|max:3000'
					);
					$uploaded = $this->checkUploadedFile ( $file, $rules, DESTINATION_PATH );
					//dd($uploaded);
					if ($uploaded) {
						$massage = 'Upload Success!';
					} else {
						$massage = 'upload fail!';
					}
				} else {
					$massage = 'File is invalid!';
				}
			}

			$this->computer ->create(Input::all());
		return Redirect::route ( 'admin.computers.index' )->with ( 'messages', $massage );
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product = $this->computer->findOrFail ( $id );
		if ($product) {
			$maker = PCMaker::all ()->last ();
			$maker = $maker->lists ( 'name', 'id' );
			// dd($maker);
			// $maker_options= \DB::table('computer_makers')->orderBy('name', 'asc')->lists('name', 'id');
			return View::make ( 'admin.editProduct', compact ( 'maker', 'product' ) );
		}
		return Redirect::route ( 'admin.computers.index' );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$massage = 'Fail !';
		if ($product = $this->computer->findOrFail ( $id )) {
			if (Input::hasFile ( 'image' )) {
				$file = Input::file ( 'image' );
				if ($file->isValid ()) {
					$rules = array (
							'file' => 'image|mime:jpg,gif,png|max:3000' 
					);
					$uploaded = $this->checkUploadedFile ( $file, $rules, DESTINATION_PATH );
					if ($uploaded) {
						$massage = 'Upload Success!';
					} else {
						$massage = 'upload fail!';
					}
				} else {
					$massage = 'File is invalid!';
				}
			}
			$product->update(Input::all());
			$product->save ();
		}
		return Redirect::route ( 'admin.computers.index' )->with ( 'messages', $massage );
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$com = $this->computer->find ( $id );

		$chekDelete = $com->delete ();
		if ($chekDelete) {
			return Redirect::route ('admin.computers.index')->with ( 'data', 'Da xoa' );
		}
	}

	public function checkUploadedFile($file, $rules, $destinationPath) {
		$validation = \Validator::make ( Input::all (), $rules );
		if ($validation->fails ()) {
			return false;
		} else {
			$name = $file->getClientOriginalName ();
			return $uploadSuccess = $file->move ( $destinationPath, $name );
			
		}
		return false;
	}
}
