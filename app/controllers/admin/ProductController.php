<?php

namespace Admin;

use View;
use Input;
use Validator;
use Redirect;
use Auth;

define ( "DESTINATION_PATH", public_path () . '/image_sp/' );
class ProductController extends \BaseController {
	
	public function listProduct() {
		$mComputer = \Computer::with ( 'pcmaker' )->get ();
		if ($mComputer !== null) {
			return View::make ( "admin.product" )->with ( 'mComputer', $mComputer );
		}
	}
	
	public function removeProduct($id) {
		$com = \Computer::find ( $id );
		$chekDelete = $com->delete ();
		if ($chekDelete) {
			return Redirect::to ( 'admin/product-manager' )->with ( 'data', 'Da xoa' );
		}
	}
	
	public function editProduct($id) {
		$product = \Computer::findOrFail ( $id );
		if ($product) {
			$maker = \PCMaker::all ()->last ();
			$maker = $maker->lists ( 'name', 'id' );
			// dd($maker);
			// $maker_options= \DB::table('computer_makers')->orderBy('name', 'asc')->lists('name', 'id');
			return \View::make ( 'admin.editProduct', compact ( 'maker', 'product' ) );
		}
		return Redirect::to ( 'admin/product-manager' );
	}
	
	// GET /new-product/
	public function addnewProduct(){
		$maker = \PCMaker::all ()->last ();
		$maker = $maker->lists ( 'name', 'id' );
		return \View::make('admin.addnew', compact('maker'));
	}
	public function insertProduct() {
		$massage = 'Fail !';
		
			if (Input::hasFile ( 'image' )) {
				$file = Input::file ( 'image' );
				if ($file->isValid ()) {
					$rules = array (
							'file' => 'image|mime:jpg,gif,png|max:3000'
					);
					$uploaded = $this->checkUploadedFile ( $file, $rules, DESTINATION_PATH );
					//dd($uploaded);
					if ($uploaded) {
						$massage = 'Upload Success!';
					} else {
						$massage = 'upload fail!';
					}
				} else {
					$massage = 'File is invalid!';
				}
			}

			$product = new \Computer();
			$product->name = Input::get ( 'name' );
			$product->conputer_maker_id = Input::get ( 'maker' );
			$product->image = $file->getClientOriginalName ();
			$product->price = Input::get ( 'price' );
			$product->weight = Input::get('weight');
			$product->date_manufacture = Input::get('date');
			$product->save ();
		return Redirect::to ( 'admin/product-manager' )->with ( 'messages', $massage );
	}
	
	public function updateProduct($id) {
		$massage = 'Fail !';
		if ($product = \Computer::findOrFail ( $id )) {
			if (Input::hasFile ( 'image' )) {
				$file = Input::file ( 'image' );
				if ($file->isValid ()) {
					$rules = array (
							'file' => 'image|mime:jpg,gif,png|max:3000' 
					);
					$uploaded = $this->checkUploadedFile ( $file, $rules, DESTINATION_PATH );
					if ($uploaded) {
						$massage = 'Upload Success!';
					} else {
						$massage = 'upload fail!';
					}
				} else {
					$massage = 'File is invalid!';
				}
			}
			$product->name = Input::get ( 'name' );
			$product->conputer_maker_id = Input::get ( 'maker' );
			$product->image = $file->getClientOriginalName ();
			$product->price = Input::get ( 'price' );
			$product->save ();
		}
		return Redirect::to ( 'admin/product-manager' )->with ( 'messages', $massage );
	}
	
	public function checkUploadedFile($file, $rules, $destinationPath) {
		$validation = \Validator::make ( Input::all (), $rules );
		if ($validation->fails ()) {
			return false;
		} else {
			$name = $file->getClientOriginalName ();
			return $uploadSuccess = $file->move ( $destinationPath, $name );
			
		}
		return false;
	}
}