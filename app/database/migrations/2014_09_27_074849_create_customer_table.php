<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table){
			$table->increments('id')->unsigned;
			$table->string('name');
			$table->string('address');
			$table->string('phone');
			$table->integer('age');
			$table->string('email');
			
			$table->softDeletes();
			$table->timestamps();
			
			$table->engine='InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
