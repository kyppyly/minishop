c<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillComputerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bill_computers', function(Blueprint $table){
			$table->integer('computer_id')->unsigned();
			$table->integer('bill_id')->unsigned();
			
			$table->primary(array('computer_id','bill_id'));
			$table->softDeletes();
			$table->timestamps();
			$table->engine='InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bill_computers');
	}

}
