<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipComputerBillUserCustomerBillComputer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::table('bills', function (Blueprint $table){
				$table	->foreign('user_id')
						->references('id')
						->on('users')
						->onDelete('cascade');
				$table	->foreign('customer_id')
						->references('id')
						->on('customers')
						->onDelete('cascade');
			
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
