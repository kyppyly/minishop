<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table){
			$table->increments('id')->unsigned;
			$table->integer('group_user_id')->unsigned();
			$table->string('email');
			$table->string('name');
			$table->string('account');
			$table->string('password');
			$table->string('password_temp');
			$table->string('code');
			$table->boolean('actived');
			
			//create 2 colum create_at and active_at Date time
			$table->timestamps();
			$table->engine= 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		
	}

}
