<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lops', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->unsignedInteger('student_id');
			$table->foreign('student_id')->references('id')->on('students');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lops');
	}

}
