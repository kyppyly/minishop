<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipComputerBillComputer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bill_computers', function (Blueprint $table){
				$table	->foreign('computer_id')
						->references('id')
						->on('computers');
				$table	->foreign('bill_id')
						->references('id')
						->on('bills');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
