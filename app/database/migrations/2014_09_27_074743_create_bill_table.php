<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bills', function(Blueprint $table){
			$table->increments('id')->unsigned;
			$table->integer('customer_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->float('total_price');
			$table->dateTime('order_date');
			
			$table->softDeletes();
			$table->timestamps();
			
			$table->engine='InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bills');
	}

}
