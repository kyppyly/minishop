<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePcmakerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			Schema::create('computer_makers', function (Blueprint $table){
			$table->increments('id')->unsigned;
			$table->string('name');
			
			$table->softDeletes();
			$table->timestamps();
			
			$table->engine='InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('computer_makers');
	}

}
