<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComputerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('computers', function(Blueprint $table){
			$table->increments('id')->unsigned;
			$table->integer('conputer_maker_id')->unsigned();
			$table->string('name', 50);
			$table->float('price');
			$table->float('weight');
			$table->string('image');
			$table->date('date_manufacture');
			
			$table->softDeletes();
			$table->timestamps();
			
			
			
			$table->engine='InnoDB';
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('computers');
	}

}
