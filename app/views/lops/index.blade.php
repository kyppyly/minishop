@extends('layouts.scaffold')

@section('main')

<h1>All Lops</h1>

<p>{{ link_to_route('lops.create', 'Add New Lop', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($lops->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($lops as $lop)
				<tr>
					<td>{{{ $lop->name }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('lops.destroy', $lop->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('lops.edit', 'Edit', array($lop->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no lops
@endif

@stop
