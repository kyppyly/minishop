@extends('admin.main')
@section('content')
	{{Form::open(array('route'=>'admin.computers.store', 'files'=>true))}}
		<div>
			{{Form::label('lblName', 'Name')}}
			{{Form::text('name')}}
		</div>
		<div>
			{{Form::label('lblPrice', 'Price')}}
			{{Form::text('price')}}
		</div>
		<div>
			{{Form::label('lblweight', 'Weight')}}
			{{Form::text('weight')}}
		</div>
		<div>
			{{Form::label('lbdate', 'Date')}}
			{{Form::text('date_manufacture')}}
		</div>
				<div>
			{{Form::label('lblMaker', 'Maker')}}
			{{Form::select('conputer_maker_id',$maker)}}
		</div>
		<div>
			{{Form::file('image')}}
		</div>
		<div>
			{{Form::submit('Insert')}}
		</div>
		
	{{Form::close()}}
@stop