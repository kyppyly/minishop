<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ADMIN PAGE</title>
</head>

<body>
<table width="40%" border="0" align="center" bgcolor="#CCCCCC">
	<form action="{{ URL::route('get-admin-login') }}" method="post">
		<tr align="center">
			<td colspan="2">LOGIN ADMIN</td>
		</tr>
		<tr>
			<td>ACCOUNT :</td>
			<td><input type="text" name="username" /></td>
			<td><label name='userError' color="red">
				@if($errors->has('username'))
				{{ $errors->first('username') }}
			@endif		
			</label></td>
		</tr>
		<tr>
			<td>PASSWORD</td>
			<td><input type="password" name="password" /></td>
			<td><label name='passError' color="red">
			@if($errors->has('password'))
				{{ $errors->first('password') }}
			@endif
			</label></td>
		</tr>
		<tr align="center">
			<td><input type="submit" name="btnLogin" value="Signin" /></td>
			<td><input type="reset" name="nhaplai" value="reset" /></td>
			<td><input type="checkbox" name='remember' >Remember</input></td>
		</tr>
		{{Form::token()}}
	</form>
</table>
</body>
</html>
