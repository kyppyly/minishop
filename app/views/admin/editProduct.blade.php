@extends('admin.main')
@section('content')
	{{Form::model([$maker, $product],array('method' => 'PATCH', 'route' => array('admin.computers.update', $product->id), 'files'=>true))}}
		<div>
			{{Form::label('lblName', 'Name')}}
			{{Form::text('name', $product->name)}}
		</div>
		<div>
			{{Form::label('lblPrice', 'Price')}}
			{{Form::text('price',  $product->price)}}
		</div>
		<div>
			{{Form::label('lblMaker', 'Maker')}}
			{{Form::select('conputer_maker_id',$maker,  $product->pcmaker->id)}}
		</div>
		<div>
			{{Form::file('image')}}
		</div>
		<div>
			{{Form::submit('Update')}}
		</div>
		
	{{Form::close()}}
@stop
