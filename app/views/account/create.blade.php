@extends('layout.main')
@section('content')
	<form action="{{URL::route('account-create-post')}}" method ="post">
		<!-- {{print_r($errors)}} -->
		<div class="field">
		Email: <input type="email" name="email" 
		@if(Input::old('email'))
			{{ "value ='". Input::old('email')."'"}}
		@else
			 {{"" }}
		@endif/>
		@if($errors->has('email'))
		{{$errors->first('email')}}
		@endif
		</div>
		
		<div class="field">
		Username: <input type="text" name="username" {{Input::old('username')? 'value="' . Input::old('username').'"': ''}}/>
		@if($errors->has('username'))
			{{$errors->first('username')}}
		@endif
		</div>
		<div class="field">
		Password: <input type="password" name="password" {{Input::old('password')? 'value="' . Input::old('password').'"': ''}}/>
		@if($errors->has('password'))
			{{$errors->first('password')}}
		@endif
		</div>
		<div class="field">
		Password again: <input type="password" name="password_again" {{Input::old('password_again')? 'value="' . Input::old('password_again').'"': ''}}/>
		@if($errors->has('password_agin'))
			{{$errors->first('password_again')}}
		@endif
		</div>
		<input type="submit" value ="Create account" />
		{{ Form::token()}}
	</form>
@stop