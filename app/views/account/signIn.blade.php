@extends('layout.main')
@section('content')
	<form action="{{URL::route('account-sign-in-post')}}" method="post">
		<div>
			<label>Email: </label><input name="email" type="email" {{Input::old('email')? 'value="'.Input::old('email').'"' : ''}}/>
			@if($errors->has('email'))
				{{ $errors->first('email') }}
			@endif
		</div>
		<div>
			<label>Password: </label><input name="password" type="password"/>
			@if($errors->has('password'))
				{{$errors->first('password')}}
			@endif
		</div>
		<div>
			<input type="checkbox" name ='remember' id ="remember"/>
			<label for="remember">Remember me</label>
		</div>
		<input type='submit' name="submit" value="Sign in" />
		{{Form::token()}}
	</form>
@stop
